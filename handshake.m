function handle_NXT = handshake

%% Preperations for MATLAB
COM_CloseNXT all                % close all handshakes
close all                       % close all figures
clear all                       % clear all variables

%% establish contact with NXT
handle_NXT = COM_OpenNXT();     % establish handshake
COM_SetDefaultNXT(handle_NXT);	% set global standard-handshake

end