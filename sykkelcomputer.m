%% Sykkelcomputer
% Gruppe 1362

%% Initialize
handle_NXT  = handshake();          % establish connection NXT
motor       = initMotor('B');       % initiate motor
OpenLight(SENSOR_1, 'ACTIVE');      % initiate light sensor

%% Variables
%%% Start values
i           = 1;

%%% Arrays
light       = zeros(1,100);

%% Plots

%% The Loop
while 1

%%% Fetch data

%%% Calculations

%%% Update plots

%%% End loop prep

i = i + 1;

end
%% Terminate connection
terminateNXT(); 