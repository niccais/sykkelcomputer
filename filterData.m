function value = filterData(data,limit)

global i;
value = data(i);
if i >= limit
    for j = 1:limit
        value = value + 1/limit * data(i-(j-1));
    end
end

end