function d = differ(value,time)
global i;

if i > 1
    d = (value(i)-value(i-1))/(toc(time(i-1)));
else
    d = 0;
end

end