function terminateNXT(mV, mH, handle_NXT)

% stopp motorer
mV.Stop;
mH.Stop;

%Steng sensor
CloseSensor(SENSOR_1);

% Clear MEX-file to release joystick
clear joymex2

% Close NXT connection.
COM_CloseNXT(handle_NXT);

end